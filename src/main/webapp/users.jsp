<%@ page import="org.iu.dspwa042101.usermanagement.User" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>Nutzer:innen</title>
</head>
<body>
<h1>Nutzer:innenliste!</h1>
<% List<User> users = (List<User>) request.getAttribute("users"); %>
<% for (User user : users) {
    out.println("<p>" + user.getFullName() + "</p>");
}%>
<p><a href="/User_Management_war_exploded/register">Registrierung</a></p>
<p><a href="/User_Management_war_exploded">Zurück zur Startseite</a></p>
</body>
</html>
